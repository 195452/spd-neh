#include <iostream>
#include <vector>
#include <fstream>
#include <limits>
#include <list>
#include <algorithm>

#define MAXSTREAMSIZE std::numeric_limits<std::streamsize>::max()

using namespace std;

class Neh {

    vector<size_t> tasks;
    vector<size_t> task_end;
    size_t m,n;

    struct ZestawZadan {
        size_t id,p;
        bool operator<(const ZestawZadan& drugi) const //brzydko, bo odwrotnie..
        {
            return (p>drugi.p)?true:false;
        }
    };
    vector<ZestawZadan> zestawy_zadan;

public:
    list<size_t> neh_order();
    size_t cmax(list<size_t> order);
    Neh(size_t m_, size_t n_)
    {
        m=m_;
        n=n_;
        task_end.resize(m_*n_);
        zestawy_zadan.resize(n);
    }
    size_t getm()
    {
        return m;
    }
    size_t getn()
    {
        return n;
    }

    size_t add_task(size_t p)  //zwraca referencje do wstawionego elementu
    {
        tasks.push_back(p);
        return tasks.back();
    }

};

size_t Neh::cmax(list<size_t> order)
{
    task_end[order.front()*m]=tasks[order.front()*m]; //pierwsze zadanie
    for(size_t i=(order.front()*m+1); i<(order.front()*m+m); i++) //raszta zadan z pierwszego rzedu
    {
        task_end[i]=tasks[i]+task_end[i-1];
    }
    auto tasks_before=order.begin(); //grupa zadan przeliczona w poprzedniej iteracji (petli z itr, ponizej)
    for(auto itr=++order.begin(); itr!=order.end(); itr++)
    {
        task_end[(*itr)*m]=tasks[(*itr)*m]+task_end[(*tasks_before)*m]; //pierwsze zadanie w danym rzedzie
        for(size_t i=((*itr)*m+1); i<(*itr)*m+m; i++) //reszta zadan w danym rzedzie
        {
            task_end[i]=max(task_end[i-1],task_end[i-((*itr)-(*tasks_before))*m])+tasks[i];
        }
        tasks_before=itr;
    }
    return task_end[order.back()*m+m-1];
}


list<size_t> Neh::neh_order()
{
    //posortowanie grup zadan wg sumy p, od najwiekszej
    for(size_t i=0;i<n;i++)
    {
        zestawy_zadan[i].p=0;
        zestawy_zadan[i].id=i;
        for(size_t j=0;j<m;j++)
            zestawy_zadan[i].p+=tasks[i*m+j];
    }
    sort(zestawy_zadan.begin(),zestawy_zadan.end());

    //ukladanie zadan
    list<size_t> task_list;
    size_t current_cmax;
    task_list.push_back(zestawy_zadan[0].id);
    for(size_t i=1;i<n;i++) //dla kazdego zadania
    {
        list<size_t>::iterator itr=task_list.begin();
        list<size_t>::iterator where_to_put;
        current_cmax=std::numeric_limits<std::size_t>::max();
        for(size_t j=0; j<=i; j++) //dla i+1 mozliwosci wstawienia
        {
            //wstawianie, sprawdzanie i usuwanie
            auto position = task_list.insert(itr,zestawy_zadan[i].id);
            size_t tmp_cmax=cmax(task_list);
            if (tmp_cmax<current_cmax)
            {
                current_cmax=tmp_cmax;
                where_to_put=itr;
            }
            task_list.erase(position);
            itr++;
        }
        task_list.insert(where_to_put,zestawy_zadan[i].id);
    }
    return task_list;
}



int main()
{
    ifstream file("neh.data.txt");
    if(file.is_open())
    {
        size_t input_data_nr=0;
        while(!file.eof())
        {
            file.ignore(MAXSTREAMSIZE,':');
            size_t n,m,cmax;
            n=0;
            vector<size_t> correct_order;
            file >> n;
            if (n!=0)
            {
                file >> m;
                Neh neh(m,n);
                for(size_t i=0;i<m*n;i++)
                {
                    size_t tmp_p;
                    file >> tmp_p;
                    neh.add_task(tmp_p);
                }
                file.ignore(MAXSTREAMSIZE,':');
                file >> cmax;
                for(size_t i=0;i<n;i++)
                {
                    size_t task;
                    file >> task;
                    correct_order.push_back(task-1);
                }
                list<size_t> calc_order=neh.neh_order();
                size_t calc_cmax = neh.cmax(calc_order);
                if(calc_cmax!=cmax)
                    cout << "Dane nr: "<<  input_data_nr << ", wynik: "<< calc_cmax << ", wynik przykladowy:  "<< cmax << "  !"<< endl;
                else
                    cout << "Dane nr: "<<  input_data_nr << ", wynik: "<< calc_cmax << ", wynik przykladowy:  "<< cmax << endl;
                input_data_nr++;
            }

        }

    }
    else
        cout << "Blad otwierania pliku";

    return 0;
}

